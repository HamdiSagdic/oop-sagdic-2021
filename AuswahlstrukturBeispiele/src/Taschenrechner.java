import java.util.Scanner;

public class Taschenrechner {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		for (;;) {
			System.out.println(
					" Geben Sie zwei Zahlen ein mit denen Sie eine mathematische Operation durchfuehren wollen!");
			System.out.println(" Zahl 1:");
			float zahl1 = myScanner.nextFloat();
			System.out.println(" Zahl 2:");
			float zahl2 = myScanner.nextFloat();
			System.out.println(" Waehlen Sie einer der mathematische Operationen : + , - , / oder *");
			char operation = myScanner.next().charAt(0);
			if (operation == '+') {
				System.out.printf("Das berechnete Ergebnis lautet: %.2f \n\n", zahl1 + zahl2);
			} else if (operation == '-') {
				System.out.printf("Das berechnete Ergebnis lautet: %.2f \n\n", zahl1 - zahl2);

			} else if (operation == '*') {
				System.out.printf("Das berechnete Ergebnis lautet: %.2f \n\n", zahl1 * zahl2);

			} else if (operation == '/') {
				System.out.printf("Das berechnete Ergebnis lautet: %.2f \n\n", zahl1 / zahl2);

			} else {
				System.out.printf("Falsche Eingabe! Wiederholen Sie Ihre Eingabe!\n\n");
			}

		}
	}

}
