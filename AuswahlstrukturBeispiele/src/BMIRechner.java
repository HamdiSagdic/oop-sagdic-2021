import java.util.Scanner;

public class BMIRechner {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Body Mass Index\n");
		System.out.println("Geschlecht m/w:");
		char geschlecht = myScanner.next().charAt(0);
		while (geschlecht != 'm' && geschlecht != 'w') {
			System.out.println("Falsche Eingabe!\n");
			System.out.println("W�hlen Sie Ihr Geschlecht!\n");
			System.out.println("(w) f�r weiblich oder (m) f�r maennlich\n");
			geschlecht = myScanner.next().charAt(0);
		}
		if (geschlecht == 'm') {
			System.out.println("Ihr Geschlecht: Maennlich\n");
			System.out.println("Koerpergewicht in kg:");
			float gewicht = myScanner.nextFloat();
			System.out.println("K�rpergroe�e in m:");
			float groesse = myScanner.nextFloat();
			float bmi = (gewicht / (groesse * groesse));
			if (bmi >= 20 && bmi <= 25) {
				System.out.printf("BMI:%.2f -> Normalgewicht\n", bmi);
			}
			if (bmi < 20) {
				System.out.printf("BMI:%.2f -> Untergewicht\n", bmi);
			}
			if (bmi > 25) {
				System.out.printf("BMI:%.2f -> Uebergewicht\n", bmi);
			}
		}
		if (geschlecht == 'w') {
			System.out.println("Ihr Geschlecht: Weiblich\n");
			System.out.println("Koerpergewicht in kg:\n");
			float gewicht = myScanner.nextFloat();
			System.out.println("K�rpergroe�e in m:\n");
			float groesse = myScanner.nextFloat();
			float bmi = (gewicht / (groesse * groesse));
			if (bmi >= 19 && bmi <= 24) {
				System.out.printf("BMI:%.2f -> Normalgewicht\n", bmi);
			}
			if (bmi < 19) {
				System.out.printf("BMI:%.2f -> Untergewicht\n", bmi);
			}
			if (bmi > 24) {
				System.out.printf("BMI:%.2f -> Uebergewicht\n", bmi);
			}
		}
	}
}
