
public class FalschesDatumException extends Exception {

	private int falscherWert;

	public FalschesDatumException(String msg, int falscherWert) {
		super(msg);
		this.falscherWert = falscherWert;
	}

	public int getFalscherWert() {
		return falscherWert;
	}
}
