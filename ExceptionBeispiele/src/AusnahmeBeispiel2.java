
public class AusnahmeBeispiel2 {
	private int[] zliste = { 1, 2, 3 };

	public static void main(String[] args) {
		AusnahmeBeispiel2 ab = new AusnahmeBeispiel2();

		try {
			System.out.println(ab.liesElement(5));
		} catch (ElementNichtVorhandenException ex) {
//			System.out.println("Element nicht vorhanden!");
			System.out.println(ex.getMessage());
			System.out.println(ex.getFpos());
		}

	}

	public int liesElement(int pos) throws ElementNichtVorhandenException {
		if (pos >= zliste.length)
			throw new ElementNichtVorhandenException("unzulässiger Index", pos);

		int erg = zliste[pos];
		return erg;
	}
}
