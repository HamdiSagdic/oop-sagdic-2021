package myUtil;

public class MyIndexException extends Exception {
	private int wrongIndex;

	public MyIndexException(int wrongIndex) {
		super("MyIndexException");
		this.wrongIndex = wrongIndex;
	}

	public int getWrongIndex() {
		return wrongIndex;
	}
}
