package myUtil;

public class MyIntArray implements IIntArray {

	private int[] arr;

	public MyIntArray(int l�nge) {
		arr = new int[l�nge];
	}

	public int get(int index) throws MyIndexException {

		if (index >= this.arr.length) {
			throw new MyIndexException(index);
		}
		return arr[index];
	}

	public void set(int index, int value) throws MyIndexException {
		if (index >= this.arr.length) {
			throw new MyIndexException(index);
		}
		arr[index] = value;
	}

	public void increase(int n) {

		int[] temp = new int[this.arr.length + n];
		for (int i = 0; i < this.arr.length; i++)
			temp[i] = this.arr[i];
		this.arr = temp;

	}

	public static void main(String[] args) throws MyIndexException {

		IIntArray arr = new MyIntArray(10);
		arr.increase(12);
		try {
			arr.set(11, 2);
		} catch (MyIndexException ex) {
			System.out.println(ex.getMessage());
			System.out.println(ex.getWrongIndex());
		}
	}

}
