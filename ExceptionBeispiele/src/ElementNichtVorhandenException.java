
public class ElementNichtVorhandenException extends Exception {
	private int fpos;

	public ElementNichtVorhandenException(String msg, int fpos) {
		super(msg);
		this.fpos = fpos;

	}

	public int getFpos() {
		return fpos;
	}
}
