
public class AusnahmeBeispiele {

	public static void main(String[] args) {

		try {
			int erg = Integer.parseInt(args[0]) + Integer.parseInt(args[1]);
			System.out.println(erg);
		}
		catch (ArrayIndexOutOfBoundsException ex) {
			System.out.println("Error: Element nicht vorhanden!");
		} 
		catch (java.lang.NumberFormatException ex) {
			System.out.println("Error: Konvertierungsfehler!");
		}
		catch (Exception ex) {
			System.out.println("Unbekannter Fehler!");
		}
	}
}
