
public class Aufgabe6 {

	public static void main(String[] args) {
		try {
			System.out.println(div(1, 0));
		} catch (ArithmeticException ex) {
			System.out.println(ex.getMessage());
		}

	}

	public static double div(double a, double b) throws ArithmeticException {
		if (b == 0) {
			throw new ArithmeticException("Error: Nicht durch 0 dividieren!");
		}
		return a / b;
	}

}