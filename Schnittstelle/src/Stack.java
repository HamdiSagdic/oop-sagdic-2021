
public class Stack implements IntStack {
	private int index;
	private int[] speicher;

	public Stack(int arrgr) {
		this.speicher = new int[arrgr];
	}
//@Override
//	public void push(int x) {
//		if (index < speicher.length) {
//			speicher[index] = x;
//			index++;
//		} else
//			System.out.println("Maximale Anzahl der Elemente erreicht!\nErh�he den Speicherplatz im Array!");
//	}

	@Override
	public void push(int x) {
		if (index >= speicher.length) {
			int[] temp = new int[this.speicher.length + 1];
			for (int i = 0; i < this.speicher.length; i++)
				temp[i] = this.speicher[i];
			this.speicher = temp;

		}
		speicher[this.index++] = x;
	}

	@Override
	public int pop() {
		if (index > 0) {
			index--;
		} else
			System.out.println("keine Elemente verf�gbar!");
		return speicher[index];

	}

	public void Stackausgabe() {
		if (index != 0) {
			System.out.print("[");
			for (int i = 0; i < index; i++) {
				if (i < index - 1) // i zwischen 0 und index
					System.out.print(speicher[i] + ",");
				if (i == (index - 1)) // i == 0
					System.out.print(speicher[i]);
			}
			System.out.println("]");
		} else
			System.out.println("[-]");
	}

}
