
public interface Form {

	double PI = 3.1415;

	double determineArea();

}
