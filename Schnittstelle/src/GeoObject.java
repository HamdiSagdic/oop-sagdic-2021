
public abstract class GeoObject {
	private double x;
	private double y;

	public GeoObject(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void move(double dx, double dy) {
		x += dx;
		y += dy;
	}

	@Override
	public String toString() {
		return "GeoObject [x=" + x + ", y=" + y + "]";
	}
}