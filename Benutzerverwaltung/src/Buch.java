
public class Buch implements Comparable<Buch> {
	private String titel;
	private String isbn;
	private String autor;

	public Buch(String titel, String isbn, String autor) {
//		super();
		this.titel = titel;
		this.isbn = isbn;
		this.autor = autor;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	@Override
	public int compareTo(Buch b) {
		return this.getIsbn().compareTo(b.getIsbn());
	}

	@Override
	public String toString() {
		return "[ " + titel + " , " + isbn + " , " + autor + "]";
	}
}
