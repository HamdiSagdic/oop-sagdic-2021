import java.util.*;

public class Buchverwaltung {

	static void menue() {
		System.out.println("\n ***** Mitarbeiterverwaltung *******");
		System.out.println(" 1) Eintragen ");
		System.out.println(" 2) Suchen ");
		System.out.println(" 3) L�schen");
		System.out.println(" 4) Die gr��te ISBN");
		System.out.println(" 5) Anzeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	}

	public static void main(String[] args) {

		List<Buch> buchliste = new ArrayList<Buch>();
		buchliste.add(new Buch(" OOP", "1234", "Jeff"));
		buchliste.add(new Buch(" Java", "1235", "Idres"));
		buchliste.add(new Buch(" DB", "1235", "Hein"));
		Scanner myScanner = new Scanner(System.in);

		char wahl;
		String eintrag;

		int index;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':
				System.out.println("Titel :");
				String titel = myScanner.next();
				System.out.println("ISBN: ");
				String isbn = myScanner.next();

				System.out.println("Autor :");
				String autor = myScanner.next();
				Buch b = new Buch(titel, isbn, autor);
				buchliste.add(b);
				break;
			case '2':
				System.out.println("die zu findene ISBN eingeben:");
				String findIsbn = myScanner.next();
				boolean contains = false;

				for (Iterator<Buch> it = buchliste.iterator(); it.hasNext();) {
					Buch b2 = it.next();
					if (b2.getIsbn().equals(findIsbn)) {
						System.out.println(b2.getTitel());
						contains = true;
					}
				}
				if (contains == false) {
					System.out.println("Buch nicht gefunden");
				}

				break;
			case '3':

				System.out.println("die zu l�schende ISBN eingeben");
				System.out.println("ISBN:");
				isbn = myScanner.next();
				Buch B = null;
				for (Buch buch : buchliste) {
					if (buch.getIsbn().equals(isbn)) {
						B = buch;
						break;
					}
				}
				if (B != null)
					buchliste.remove(B);
				else
					System.out.println("Buch ist nicht vorhanden!");
				break;

			case '4':
				Collections.sort(buchliste);
				System.out.println(buchliste.get(buchliste.size() - 1));
				break;
			case '5':
				System.out.println(buchliste);
				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
			}

		} while (wahl != 9);

	}

}
