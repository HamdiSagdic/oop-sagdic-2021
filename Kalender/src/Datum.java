public class Datum {

	// Attribute

	protected int tag;
	protected int monat;
	protected int jahr;

	// Konstruktor
	public Datum() {

		this.tag = 01;
		this.monat = 01;
		this.jahr = 1970;
	}

	public Datum(int Tag, int Monat, int Jahr) {
		setTag(Tag);
		setMonat(Monat);
		setJahr(Jahr);
	}

	// Methoden
	public static int quartalrechner(Datum date) {
		int x = 0;
		if (date.getMonat() <= 3 && date.getMonat() > 0)
			x = 1;
		else if (date.getMonat() <= 6 && date.getMonat() > 3)
			x = 2;
		else if (date.getMonat() <= 9 && date.getMonat() > 6)
			x = 3;
		else if (date.getMonat() <= 12 && date.getMonat() > 9)
			x = 4;
		return x;

	}

	// Setter und Getter

	public void setTag(int Tag) {
		this.tag = Tag;
	}

	public int getTag() {
		return this.tag;
	}

	public void setMonat(int Monat) {
		this.monat = Monat;
	}

	public int getMonat() {
		return this.monat;
	}

	public void setJahr(int Jahr) {
		this.jahr = Jahr;
	}

	public int getJahr() {
		return this.jahr;
	}

	@Override
	public String toString() {
		return "" + tag + "." + monat + "." + jahr;
	}

	public boolean equals(Object obj) {

		if (obj == null)
			return false;

		Datum d = (Datum) obj;
		if (this.tag == d.getTag() && this.monat == d.getMonat() && this.jahr == d.getJahr())
			return true;
		else
			return false;

	}
}