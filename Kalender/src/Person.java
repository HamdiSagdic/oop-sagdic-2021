
public class Person {

	private String name;
	private Datum geb; // aggregation implementiert
	private static int anzahl = 0;
	private int id;

	public Person() {

		this.name = null;
		this.geb = null;
		setId(anzahl + 1);
		anzahl++;
	}

	public Person(String name, Datum geb) {
		setName(name);
		setGeb(geb);
		setId(anzahl + 1);
		anzahl++;

	}

	public void setId(int id) {
		this.id = id;

	}

	public void setGeb(Datum geb) {
		this.geb = geb;
	}

	public Datum getGeb() {
		return geb;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public static int getAnzahl() {
		return anzahl;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Name:" + name + ", Geburtsdatum:" + geb + ", ID " + id;
	}
}
