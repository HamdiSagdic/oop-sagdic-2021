public class Program {

	public static void main(String[] args) {

		Datum date1 = new Datum();
		date1.setTag(01);
		date1.setMonat(01);
		date1.setJahr(1970);
		System.out.println(date1);

		Datum date2 = new Datum(01, 01, 1970);
		System.out.println(date2);


		
		if (date1.equals(date2))
			System.out.println("Datum ist gleich");
		else
			System.out.println("Datum ist ungleich");

		System.out.println(Datum.quartalrechner(date1));
		System.out.println("-------------------------------------");
		System.out.println("-------------------------------------");
		////////////////////////////////////////////////////////////////

		KomplexeZahlen c1 = new KomplexeZahlen();
		c1.setReal(1);
		c1.setImag(2);

		KomplexeZahlen c2 = new KomplexeZahlen(-1, -2);

		System.out.println(c1);
		System.out.println(c2);

		if (c1.equals(c2))
			System.out.println("Komplexe Zahlen sind gleich");
		else
			System.out.println("Komplexe Zahlen sind ungleich");

		KomplexeZahlen.multiply(c1, c2);
		c1.multiply2(c1, c2);

		System.out.println("-------------------------------------");
		System.out.println("-------------------------------------");

		///////////////////// Aufgabe 3.4//////////////

		Person p1 = new Person("Tom", new Datum(03, 04, 1990));
		Person p2 = new Person();
		Person p3 = new Person();

		p2.setName("Henry");
		Datum d1 = new Datum(02, 10, 1995);
		p2.setGeb(d1);
	
		
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
///////////////////// Vererbungsaufgabe//////////////
		System.out.println("-------------------------------------");
		System.out.println("-------------------------------------");
	

	}
}
