
public class KomplexeZahlen {

	private float real;
	private float imag;

	public KomplexeZahlen() {
	}

	public KomplexeZahlen(float realteil, float imaginärteil) {
		this.real = realteil;
		this.imag = imaginärteil;
	}

	public float getReal() {
		return real;
	}

	public void setReal(float real) {
		this.real = real;
	}

	public float getImag() {
		return imag;
	}

	public void setImag(float imag) {
		this.imag = imag;
	}

	public boolean equals(Object obj) {

		if (obj == null)
			return false;

		KomplexeZahlen c = (KomplexeZahlen) obj;
		if (this.real == c.getReal() && this.imag == c.getImag())
			return true;
		else
			return false;

	}

	@Override
	public String toString() {
		// return this.real + " + " + this.imag + "i";
		return String.format("%+.2f %+.2fi ", this.real, this.imag);
	}

	public static void multiply(KomplexeZahlen c1, KomplexeZahlen c2) {
		float a = c1.getReal() * c2.getReal() - c1.getImag() * c2.getImag();
		float b = c1.getReal() * c2.getImag() + c2.getReal() * c1.getImag();
		System.out.printf("%.2f %+.2fi\n", a, b);
	}

	public void multiply2(KomplexeZahlen c1, KomplexeZahlen c2) {
		float a = c1.getReal() * c2.getReal() - c1.getImag() * c2.getImag();
		float b = c1.getReal() * c2.getImag() + c2.getReal() * c1.getImag();
		System.out.printf("%.2f %+.2fi\n", a, b);

	}

}
