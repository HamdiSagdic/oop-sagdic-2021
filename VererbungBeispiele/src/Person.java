
public class Person {

	protected String name;
	protected int alter;

	public Person() {
		this.name = "Unbekannt";
		this.alter = 0;
	}

	public Person(String name, int alter) {

		setName(name);
		setAlter(alter);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAlter() {
		return alter;
	}

	public void setAlter(int alter) {
		if (alter > 0)
			this.alter = alter;
	}

	@Override
	public String toString() { // [ Name: Max , Alter: 18 ]
		return "Name: " + this.name + " , Alter: " + this.alter;
	}
}
