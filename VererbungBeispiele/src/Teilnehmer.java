
public class Teilnehmer extends Person {

	private String status;
	
	public Teilnehmer() {
		super();
		setStatus(status);
		
	}
	
	public Teilnehmer(String name, int alter, String status) {
		super(name, alter);
		this.status = status;
		//setStatus(status);
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() { // [ Name: Max , Alter: 18 ]
		return super.toString()+ " , Status:"+this.status;
	}
	
}
