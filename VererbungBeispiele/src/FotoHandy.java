
public class FotoHandy extends Handy {
	private int pixel;
	private String firma;
	public static String klassenName = "FotoHandy";

	public FotoHandy(String firma1, String typ, int pixel, String firma2) {
		super(firma1, typ);
		this.pixel = pixel;
		this.firma = firma2;
	}

	public int getPixel() {
		return pixel;
	}

	public void setPixel(int pixel) {
		this.pixel = pixel;
	}

	public String getFirma() {
		return firma;
	}

	public void setFirma(String firma) {
		this.firma = firma;
	}

	public String toString() {
		return "FotoHandy [firma(super)=" + super.getFirma() + ", typ=" + getTyp() + ", pixel=" + pixel + ", firma="
				+ firma + "]";
	}

	public static void main(String[] args) {
		//Aufgabe aus der Klasse Handy =  Handy [firma=Nokia1, typ=C62]
		Handy h = new Handy("Nokia1", "C62");
		System.out.println("h: " + h);
		//Ausgabe aus Klasse Fotohandy ( mit String.to aus klasse Handy)
		FotoHandy f = new FotoHandy("Nokia1", "TS8", 5000000, "Nokia2");
		System.out.println("f: " + f);
		//Ausgabe aus klasse  fotohandy 
		System.out.println("f.super: " + ((Handy) f).toString());
		//ausgabe fotohandy
		System.out.println("f.klassenName: " + f.klassenName);
		//ausgabe handy
		System.out.println("f.klassenName: " + ((Handy) f).klassenName);
	}
}
