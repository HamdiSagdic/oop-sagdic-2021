
public class Feiertag extends Datum {

	private String feiertagsname;

	
	public Feiertag() {
		super();
		setFeiertagsname(feiertagsname);
		
	}
	
	public Feiertag(int Tag, int Monat,int Jahr, String feiertagsname) {
		super(Tag, Monat, Jahr);
		this.feiertagsname = feiertagsname;
	}

	public String getFeiertagsname() {
		return feiertagsname;
	}

	public void setFeiertagsname(String feiertagsname) {
		this.feiertagsname = feiertagsname;
	}

	@Override
	public String toString() {
		return super.toString()+ ", (" +this.feiertagsname+ ")" ;
	}

}
