import java.util.Scanner;

public class Dispokonto extends Bankkonto {

	private double dispokredit;

	public Dispokonto() {
		super();
		setDispokredit(dispokredit);

	}

	public Dispokonto(String kontoinhaber, double kontostand, double dispokredit) {
		super(kontoinhaber, kontostand);
		this.dispokredit = dispokredit;
		
	}

	public double getDispokredit() {
		return dispokredit;
	}

	public void setDispokredit(double dispokredit) {
		this.dispokredit = dispokredit;
	}

	@Override
	public double auszahlen(double betrag) {
		
		if (getKontostand() > -this.dispokredit && getKontostand() > betrag-this.dispokredit) {
			setKontostand(getKontostand() - betrag);
			System.out.println("");
			System.out.println("---------------Auszahlung------------------");
			System.out.printf("Sie haben %.2f� von Ihrem Konto ausgezahlt\n", betrag);
			System.out.printf("Ihr aktueller Kontostand lautet: %.2f�\n", getKontostand());
			System.out.println("---------------------------------------------");
			System.out.println("---------------------------------------------");
			System.out.println("");
			

		}

		else {
			System.out.println("");
			System.out.println("-------------------Fehler--------------------");
			System.out.format("Sie m�chten %.2f� abheben\n", betrag);
			System.out.printf("Sie k�nnen Ihr Konto nicht weiter �berziehen! Aktueller Dispokredit:%.2f�\n",this.dispokredit);
			System.out.printf("Ihr aktueller Kontostand lautet: %.2f�\n", getKontostand());
			System.out.printf("Sie k�nnen maximal %.2f� abheben\n", getKontostand()+this.dispokredit);
			System.out.println("---------------------------------------------");
			System.out.println("---------------------------------------------");
			System.out.println("");
		}
		return betrag;
	}

	@Override
	public String toString() {
		return super.toString() + String.format(", Dispokredit: %.2f�", getDispokredit());
	}
}
