import java.util.Scanner;

public class Bankkonto {

	private String kontoinhaber;
	private int kontonummer;
	private double kontostand;

	public Bankkonto() {

		setKontoinhaber(kontoinhaber);
		this.kontonummer = erzeugeKontonummer();
		setKontostand(kontostand);
	}

	public Bankkonto(String kontoinhaber, double kontostand) {
		this.kontoinhaber = kontoinhaber;
		this.kontonummer = erzeugeKontonummer();
		this.kontostand = kontostand;
	}

	public String getKontoinhaber() {
		return kontoinhaber;
	}

	public void setKontoinhaber(String kontoinhaber) {
		this.kontoinhaber = kontoinhaber;
	}

	public double getKontonummer() {
		return kontonummer;
	}

	public double getKontostand() {
		return kontostand;
	}

	public void setKontostand(double kontostand) {
		this.kontostand = kontostand;
	}

	public void einzahlen(double betrag) {

		setKontostand(this.kontostand + betrag);
		System.out.println("");
		System.out.println("-----------------Einzahlung------------------");
		System.out.printf("Sie haben %.2f� auf Ihr Konto eingezahlt\n", betrag);
		System.out.printf("Ihr aktueller Kontostand lautet: %.2f�\n", getKontostand());
		System.out.println("---------------------------------------------");
		System.out.println("---------------------------------------------");
		System.out.println("");
	
	}

	public double auszahlen(double betrag) {
		
//Aufgabenstellung =  Ein- und Auszahlung ohne Beschr�nkung

//		if (this.kontostand >= betrag) {
		//
			setKontostand(this.kontostand - betrag);
			System.out.println("");
			System.out.println("---------------Auszahlung------------------");
			System.out.printf("Sie haben %.2f� von Ihrem Konto ausgezahlt\n", betrag);
			System.out.printf("Ihr aktueller Kontostand lautet: %.2f�\n", getKontostand());
			System.out.println("---------------------------------------------");
			System.out.println("---------------------------------------------");
			System.out.println("");	
			
//		} 		
//		else {
//			System.out.println("");
//			System.out.println("-------------------Fehler--------------------");
//			System.out.format("Sie m�chten %.2f� abheben\n",betrag);
//			System.out.println("Sie k�nnen Ihr Konto nicht �berziehen!");
//			System.out.printf("Ihr aktueller Kontostand lautet: %.2f�\n", getKontostand());
//			System.out.printf("Sie k�nnen maximal %.2f� abheben\n", getKontostand());
//			System.out.println("---------------------------------------------");
//			System.out.println("");
//		}
		return betrag;
	}

	@Override
	public String toString() {
		return String.format("Kontoinhaber: %s, Kontonummer: %d, Kontostand: %.2f�", this.kontoinhaber, this.kontonummer, this.kontostand);

	}

	public int erzeugeKontonummer() {
		int Min = 10000;
		int Max = 99999;
		int kontonummer = (int) (Min + (Math.random() * ((Max - Min) + 1)));

		return kontonummer;

	}
}
