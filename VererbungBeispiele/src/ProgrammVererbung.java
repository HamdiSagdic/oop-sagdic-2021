import java.util.Scanner;

public class ProgrammVererbung {

	private static Bankkonto[] konto = { new Bankkonto("Henry", 500), new Bankkonto("Exe", 1000),
			new Dispokonto("Karl", 500, 1000), new Dispokonto("Donald", 600, 2000) };

	public static void main(String[] args) {
		System.out.println("------Aufgabe4.1------");
		System.out.println("");
		Feiertag date3 = new Feiertag(24, 12, 2014, "heiligabend");
		System.out.println(date3);
		System.out.println("----------------");
		System.out.println("");
		System.out.println("");

		System.out.println("------Aufgabe4.2------");
		System.out.println(konto[0]);
		konto[0].einzahlen(500);
		konto[0].auszahlen(4000);
		System.out.println(konto[3]);
		konto[3].einzahlen(500);
		konto[3].auszahlen(500);
		konto[3].auszahlen(20000);

	}

}
